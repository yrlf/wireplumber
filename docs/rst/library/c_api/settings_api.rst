.. _settings_api:

Settings
========
.. graphviz::
  :align: center

   digraph inheritance {
      rankdir=LR;
      GObject -> WpObject;
      WpObject -> WpSettings;
   }

.. doxygenstruct:: WpSettings

.. doxygengroup:: wpsettings
   :content-only:
